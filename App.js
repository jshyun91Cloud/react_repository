import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    var helloWorld = "리액트에 오신 여러분을 환영합니다."
    return (
      <div className="Apps">
        <h2>{helloWorld}</h2>
      </div>
    );
  }
}

export default App;
